package id.ac.ui.cs.advprog.tutorial1.observer.service;

import id.ac.ui.cs.advprog.tutorial1.observer.core.*;
import id.ac.ui.cs.advprog.tutorial1.observer.repository.QuestRepository;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GuildServiceImpl implements GuildService {
        private final QuestRepository questRepository;
        private final Guild guild;
        private final Adventurer agileAdventurer;
        private final Adventurer knightAdventurer;
        private final Adventurer mysticAdventurer;

        public GuildServiceImpl(QuestRepository questRepository) {
                this.questRepository = questRepository;
                this.guild = new Guild();
                agileAdventurer = new id.ac.ui.cs.advprog.tutorial1.observer.core.AgileAdventurer(guild);
                mysticAdventurer = new id.ac.ui.cs.advprog.tutorial1.observer.core.MysticAdventurer(guild);
                knightAdventurer = new id.ac.ui.cs.advprog.tutorial1.observer.core.KnightAdventurer(guild);

                this.guild.add(agileAdventurer);
                this.guild.add(mysticAdventurer);
                this.guild.add(knightAdventurer);
        }

        @Override
        public void addQuest(Quest quest) {
                Quest savedQuest = questRepository.save(quest);
                if (savedQuest != null && savedQuest.getType() != null && savedQuest.getTitle() != null) {
                        this.guild.addQuest(savedQuest);
                }

        }

        @Override
        public List<Adventurer> getAdventurers() {
                return this.guild.getAdventurers();
        }
}
