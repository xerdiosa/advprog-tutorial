package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

        public MysticAdventurer(Guild guild) {
                this.name = "Mystic";
                this.guild = guild;
                this.guild.add(this);
        }

        @Override
        public void update() {
                Quest newQuest = this.guild.getQuest();
                if (!this.guild.getQuestType().equals("R")) {
                        this.getQuests().add(newQuest);
                }
        }
}
