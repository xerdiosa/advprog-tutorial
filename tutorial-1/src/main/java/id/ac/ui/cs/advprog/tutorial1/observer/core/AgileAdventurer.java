package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

        public AgileAdventurer(Guild guild) {
                this.name = "Agile";
                this.guild = guild;
                this.guild.add(this);
        }

        @Override
        public void update() {
                Quest newQuest = this.guild.getQuest();
                if (!this.guild.getQuestType().equals("E")) {
                        this.getQuests().add(newQuest);
                }
        }
}
